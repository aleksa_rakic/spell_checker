// Implements a dictionary's functionality
#include "dictionary.h"

// returns character index in the alphabet
int getPosition(char c)
{
    if (c >= 'a' && c <= 'z')
        return c - 'a';
    else if (c >= 'A' && c <= 'Z')
        return c - 'A';
    else if (c == '\'')
        return 26;
    else
        return -1;
}

bool is_valid_character(char c)
{
    if (c == '\'' || (c >= 'a' && c <= 'z') || (c >= 'A' && c <= 'Z'))
        return true;
    else
        return false;
}

// ###
// CHECK

bool traverse(struct node *nodeArg, const char *word, int i)
{
    int charAlphaIndex = getPosition(word[i]);
    // if current character doesn't exist in the trie, return false
    node *subNode = nodeArg->children[charAlphaIndex];

    if (subNode == NULL)
        return false;

    // calls function recurivelly until the last character in word
    i++;
    if (is_valid_character(word[i]))
    {
        return traverse(subNode, word, i);
    };

    // returns true or false depending if the word has been found
    return subNode->is_word; // line 42
}

// Returns true if word is in dictionary else false
bool check(const char *word)
{
    int i = 0;
    return traverse(ROOT, word, i);
}

// ###
// LOAD

bool insert(int i, char *word, struct node *nodeArg)
{
    int charAlphaIndex = getPosition(word[i]);

    if (is_valid_character(word[i]))
    {
        node *subNode = nodeArg->children[charAlphaIndex];
        if (subNode == NULL) // line 62
        {
            // malloc error handling
            subNode = calloc(NODE_SIZE, 1);

            nodeArg->children[charAlphaIndex] = subNode;
        }
        i++;
        if (word[i] == '\n' || word[i] == '\0')
        {
            subNode->is_word = true;
        }
        return insert(i, word, subNode);
    }
    return true;
}

// Loads dictionary into memory, returning true if successful else false
bool load(const char *dictionary)
{
    // opens file for reading
    FILE *file = fopen(dictionary, "r");
    char word[LENGTH];

    if (file == NULL)
    {
        printf("Could not open %s.\n", dictionary);
        return false;
    }

    int i = 0;
    // malloc root node
    ROOT = calloc(NODE_SIZE, 1);
    // word count in the dictionary
    WORD_COUNT = 0;
    // read word by word from file until the end of the file
    while (fgets(word, LENGTH + 1, file) != NULL)
    {
        // skip blank lines
        if (word[0] == '\n')
            continue;

        WORD_COUNT++;
        // true if whole dictionary (all words) is loaded into memory, else false
        IS_LOADED = insert(i, word, ROOT);
    };
    fclose(file);
    return IS_LOADED;
}

// Returns number of words in dictionary if loaded else 0 if not yet loaded
unsigned int size(void)
{
    if (IS_LOADED)
        return WORD_COUNT;
    else
        return 0;
}

// UNLOAD

void free_node(node *nodeArg)
{
    for (int i = 0; i < 27; i++)
    {
        node *sub_node = nodeArg->children[i];
        if (sub_node != NULL)
            free_node(sub_node);

        free(sub_node);
    }
}


// Unloads dictionary from memory, returning true if successful else false
bool unload(void)
{
    free_node(ROOT);
    free(ROOT);
    return true;
}
